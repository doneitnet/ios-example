//
//  IZMyOrdersViewController.h
//  Jabrool
//
//  Created by Линник Александр on 25.10.16.
//  Copyright © 2016 Alex Linnik. All rights reserved.
//

#import "IZBaseViewController.h"
#import "IZCourierMainScreenStateClient.h"

@interface IZCourierMainScreen : IZBaseViewController <IZCourierMainScreenStateClient>

@end
