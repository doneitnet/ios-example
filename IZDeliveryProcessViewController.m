//
//  IZDeliveryProcessViewController.m
//  Jabrool
//
//  Created by Ilya Denisov on 1/24/17.
//  Copyright © 2017 Alex Linnik. All rights reserved.
//

#import "IZDeliveryProcessViewController.h"

@interface IZDeliveryProcessViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *destinationButton;
@property (weak, nonatomic) IBOutlet UIButton *requestDetailsButton;
@property (weak, nonatomic) IBOutlet UIButton *callButton;
@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@end

@implementation IZDeliveryProcessViewController

#pragma mark - Lifecycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self _setup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setters -

- (void)setState:(IZDeliveryProcessViewControllerState)state {
    _state = state;
    
    [self _updateTitleLabel];
    [self _updateDestinationButton];
    [self _updateCallButton];
    [self _updateChatButton];
    [self _updateCancelButton];
}

- (void)setAddress:(NSString *)address {
    _address = address;
    
    [self _updateSubtitleLabel];
}

#pragma mark - Actions -

- (IBAction)openRequestDetail:(id)sender {
    if ([self.delegate respondsToSelector:@selector(deliveryProcessViewControllerWantOpenRequestDetail:)]) {
        [self.delegate deliveryProcessViewControllerWantOpenRequestDetail:self];
    }
}

- (IBAction)didPressCallButton:(id)sender {
    if ([self.delegate respondsToSelector:@selector(deliveryProcessViewControllerWantCall:)]) {
        [self.delegate deliveryProcessViewControllerWantCall:self];
    }
}

- (IBAction)didPressDestinationButton:(id)sender {
    if ([self.delegate respondsToSelector:@selector(deliveryProcessViewControllerDidPressDestination:)]) {
        [self.delegate deliveryProcessViewControllerDidPressDestination:self];
    }
}

- (void)didTapHeaderView:(id)sender {
    if ([self.delegate respondsToSelector:@selector(deliveryProcessViewControllerDidTapHeader:)]) {
        [self.delegate deliveryProcessViewControllerDidTapHeader:self];
    }
}

- (IBAction)didPressCancel:(id)sender {
    if ([self.delegate respondsToSelector:@selector(deliveryProcessViewControllerDidPressCancel:)]) {
        [self.delegate deliveryProcessViewControllerDidPressCancel:self];
    }
}

- (IBAction)didPressChat:(id)sender {
    if ([self.delegate respondsToSelector:@selector(deliveryProcessViewControllerDidPressChat:)]) {
        [self.delegate deliveryProcessViewControllerDidPressChat:self];
    }
}

#pragma mark - Private -

- (void)_setup {
    [self _updateTitleLabel];
    [self _updateSubtitleLabel];
    [self _updateDestinationButton];
    [self _updateRequestDetailsButton];
    [self _addTapGestureRecognizerToHeaderView];
}

- (void)_updateTitleLabel {
    UIColor *textColor = [UIColor blackColor];
    NSString *text = @"";
    switch (self.state) {
        case IZDeliveryProcessViewControllerStatePickUp:
            textColor = IZColorGreen;
            text = LS(kDeliveryPickupLocationTextKey);
            break;
        case IZDeliveryProcessViewControllerStateReturn:
            textColor = IZColorGrayishBrown;
            text = LS(kDeliveryReturnLocationTextKey);
            break;
        case IZDeliveryProcessViewControllerStateDropOff:
            textColor = IZColorRed;
            text = LS(kDeliveryDropOffLocationTextKey);
            break;
    }
    self.titleLabel.text = text;
    self.titleLabel.textColor = textColor;
}

- (void)_updateSubtitleLabel {
    self.subtitleLabel.text = self.address;
}

- (void)_updateDestinationButton {
    NSString *text = @"";
    switch (self.state) {
        case IZDeliveryProcessViewControllerStatePickUp:
            text = LS(kDeliveryAtPickupButtonTitleKey);
            break;
        case IZDeliveryProcessViewControllerStateReturn:
            text = LS(kDeliveryAtDestinationButtonTitleKey);
            break;
        case IZDeliveryProcessViewControllerStateDropOff:
            text = LS(kDeliveryAtDestinationButtonTitleKey);
            break;
    }
    NSString *title = text;;
    [self.destinationButton setTitle:title forState:UIControlStateNormal];
}

- (void)_updateCallButton {
    NSString *text = @"";
    switch (self.state) {
        case IZDeliveryProcessViewControllerStatePickUp:
            text = LS(kDeliveryCallRequestorButtonTitleKey);
            break;
        case IZDeliveryProcessViewControllerStateReturn:
            text = LS(kDeliveryCallReceiverButtonTitleKey);
            break;
        case IZDeliveryProcessViewControllerStateDropOff:
            text = LS(kDeliveryCallReceiverButtonTitleKey);
            break;
    }
    NSString *title = text;
    [self.callButton setTitle:title forState:UIControlStateNormal];
}

- (void)_updateChatButton {
    self.chatButton.hidden = self.state == IZDeliveryProcessViewControllerStateReturn;
    [self.chatButton setTitle:LS(kDeliveryChatButtonTitleKey) forState:UIControlStateNormal];
}

- (void)_updateCancelButton {
    self.cancelButton.hidden = self.state != IZDeliveryProcessViewControllerStateReturn;
    [self.cancelButton setTitle:LS(kDeliveryCancelButtonTitleKey) forState:UIControlStateNormal];
}

- (void)_updateRequestDetailsButton {
    [self.requestDetailsButton setTitle:LS(kDeliveryRequestDetailsButtonTitleKey) forState:UIControlStateNormal];
}

- (void)_addTapGestureRecognizerToHeaderView {
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapHeaderView:)];
    [self.headerView addGestureRecognizer:tapGestureRecognizer];
}

@end
