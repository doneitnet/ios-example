//
//  IZChangesInPackageDataDisplayManager.h
//  Jabrool
//
//  Created by Ilya Denisov on 1/25/17.
//  Copyright © 2017 Alex Linnik. All rights reserved.
//

#import <Foundation/Foundation.h>

@class IZChangesInPackageDataDisplayManager;

@protocol IZChangesInPackageDataDisplayManagerDataSource <NSObject>

- (NSInteger)changesInPackageDataDisplayManager:(IZChangesInPackageDataDisplayManager *)dataDisplayManager quantityForType:(IZPackageSizeType)sizeType;

@end

@protocol IZChangesInPackageDataDisplayManagerDelegate <NSObject>

- (void)changesInPackageDataDisplayManager:(IZChangesInPackageDataDisplayManager *)dataDisplayManager wantSelectLocationWithCompletion:(void(^)(IZLocation *))block;
- (void)changesInPackageDataDisplayManagerDidAccept:(IZChangesInPackageDataDisplayManager *)dataDisplayManager;

@end

@interface IZChangesInPackageDataDisplayManager : NSObject <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) id <IZChangesInPackageDataDisplayManagerDataSource> dataDisplayManagerDataSource;
@property (weak, nonatomic) id <IZChangesInPackageDataDisplayManagerDelegate> delegate;

- (instancetype)initWithTableView:(UITableView *)tableView;
- (void)updateTypes:(NSArray <IZPackageType *> *)types;

- (NSInteger)getSmallPackageCount;
- (NSInteger)getMediumPackageCount;
- (NSInteger)getLargePackageCount;

@end
