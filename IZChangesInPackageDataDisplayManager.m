//
//  IZChangesInPackageDataDisplayManager.m
//  Jabrool
//
//  Created by Ilya Denisov on 1/25/17.
//  Copyright © 2017 Alex Linnik. All rights reserved.
//

#import "IZChangesInPackageDataDisplayManager.h"
#import "IZChangesInPackageObjectBuilderFactory.h"
#import "IZChangesInPackageFooterView.h"
#import "IZPackageDescriptionCell.h"
#import "IZDeliveryAddressCell.h"

#define IZGetPackageTypeNumberOfRow(x)          (x+1)

static int const IZNumberOfSections             = 2;
static int const IZDeliveryAddressNumberOfRows  = 1;
static CGFloat const IZHeightForHeader          = 14;

typedef NS_ENUM(NSInteger, IZPackageDescriptionRowNumber) {
    IZPackageDescriptionRowNumberSmall = 1,
    IZPackageDescriptionRowNumberMedium,
    IZPackageDescriptionRowNumberLarge
};

typedef NS_ENUM(NSInteger, IZSectionNumber) {
    IZSectionNumberDeliveryAddress,
    IZSectionNumberPackageType
};

@interface IZChangesInPackageDataDisplayManager () <IZDeliveryAddressCellDelegate, IZChangesInPackageFooterViewDelegate>

@property (strong, nonatomic) NSMutableArray *dataSource;
@property (weak, nonatomic) UITableView *tableView;

@end

@implementation IZChangesInPackageDataDisplayManager

#pragma mark - INITIALIZER -

- (instancetype)initWithTableView:(UITableView *)tableView {
    self = [super init];
    if (self) {
        _tableView = tableView;
        [IZChangesInPackageObjectBuilderFactory registerCellsForTableView:tableView];
        tableView.dataSource = self;
        tableView.delegate = self;
    }
    return self;
}

#pragma mark - Getters -

- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataSource;
}

#pragma mark - Public -

- (void)updateTypes:(NSArray <IZPackageType *> *)types {
    [self.dataSource removeAllObjects];
    [self.dataSource addObjectsFromArray:types];
}

- (NSInteger)getSmallPackageCount {
    return [self _getQuatityValueFromPackageDescriptionCellInRow:IZPackageDescriptionRowNumberSmall];
}

- (NSInteger)getMediumPackageCount {
    return [self _getQuatityValueFromPackageDescriptionCellInRow:IZPackageDescriptionRowNumberMedium];
}

- (NSInteger)getLargePackageCount {
    return [self _getQuatityValueFromPackageDescriptionCellInRow:IZPackageDescriptionRowNumberLarge];
}

#pragma mark - <IZChangesInPackageFooterViewDelegate> -

- (void)changesInPackageFooterViewDidAccept:(IZChangesInPackageFooterView *)view {
    if ([self.delegate respondsToSelector:@selector(changesInPackageDataDisplayManagerDidAccept:)]) {
        [self.delegate changesInPackageDataDisplayManagerDidAccept:self];
    }
}

#pragma mark - <IZDeliveryAddressCellDelegate> -

- (void)deliveryAddressCellWantChangeDeliveryAddress:(IZDeliveryAddressCell *)cell {
    if ([self.delegate respondsToSelector:@selector(changesInPackageDataDisplayManager:wantSelectLocationWithCompletion:)]) {
        [self.delegate changesInPackageDataDisplayManager:self wantSelectLocationWithCompletion:^(IZLocation *location) {
            cell.textField.text = location.address;
        }];
    }
}

#pragma mark - <UITableViewDataSource> -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return IZNumberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == IZSectionNumberDeliveryAddress) {
        return IZDeliveryAddressNumberOfRows;
    }
    return IZGetPackageTypeNumberOfRow(self.dataSource.count);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id <IZCellObjectBuilder> builder = [self _objectBuilderForTableView:tableView indexPath:indexPath];
    id item = [self _itemForIndexPath:indexPath];
    UITableViewCell *cell = [builder cellForItem:item withTableView:tableView delegate:self];
    if ([cell isKindOfClass:[IZPackageDescriptionCell class]]) {
        [self _updateValueForPackageDescriptionCell:(IZPackageDescriptionCell *)cell withPackageType:item];
    }
    return cell;
}

#pragma mark - <UITableViewDelegate> -

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    id <IZCellObjectBuilder> builder = [self _objectBuilderForTableView:tableView indexPath:indexPath];
    return [builder height];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return IZHeightForHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == IZSectionNumberDeliveryAddress) {
        return CGFLOAT_MIN;
    }
    return [IZChangesInPackageFooterView height];
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == IZSectionNumberDeliveryAddress) {
        return nil;
    }
    IZChangesInPackageFooterView *footer = [IZChangesInPackageFooterView footerView];
    footer.delegate = self;
    return footer;
}

#pragma mark - Private -

- (id <IZCellObjectBuilder>)_objectBuilderForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    id <IZCellObjectBuilder> builder = nil;
    if (indexPath.section == IZSectionNumberDeliveryAddress) {
        builder = [IZChangesInPackageObjectBuilderFactory deliveryAddressObjectBuilderWithTableView:tableView];
    } else if (indexPath.section == IZSectionNumberPackageType) {
        if (indexPath.row == 0) {
            builder = [IZChangesInPackageObjectBuilderFactory packageTypeObjectBuilderWithTableView:tableView];
        } else {
            id item = [self.dataSource objectAtIndex:indexPath.row - 1];
            builder = [IZChangesInPackageObjectBuilderFactory objectBuilderForItem:item withTableView:tableView];
        }
    }
    return builder;
}

- (id)_itemForIndexPath:(NSIndexPath *)indexPath {
    id item = nil;
    if (indexPath.section == IZSectionNumberPackageType && indexPath.row > 0) {
        item = [self.dataSource objectAtIndex:indexPath.row - 1];
    }
    return item;
}

- (void)_updateValueForPackageDescriptionCell:(IZPackageDescriptionCell *)cell withPackageType:(IZPackageType *)packageType {
    if ([self.dataDisplayManagerDataSource respondsToSelector:@selector(changesInPackageDataDisplayManager:quantityForType:)]) {
        NSInteger value = [self.dataDisplayManagerDataSource changesInPackageDataDisplayManager:self quantityForType:packageType.sizeType];
        cell.quantitySelector.value = value;
    }
}

- (NSInteger)_getQuatityValueFromPackageDescriptionCellInRow:(NSInteger)row {
    NSIndexPath *path = [NSIndexPath indexPathForRow:row inSection:IZSectionNumberPackageType];
    IZPackageDescriptionCell *cell = [self.tableView cellForRowAtIndexPath:path];
    return cell.quantitySelector.value;
}


@end
