//
//  IZMyOrdersViewController.m
//  Jabrool
//
//  Created by Линник Александр on 25.10.16.
//  Copyright © 2016 Alex Linnik. All rights reserved.
//

#import "IZCourierMainScreen.h"
#import "UICheckbox.h"
#import "IZBaseViewController+Protected.h"
#import "IZBadgeBarButtonItem.h"
#import "IZCourierMainScreenState.h"
#import "IZCourierMainScreenInvisibleState.h"
#import "IZCourierMainScreenVisibleState.h"
#import "IZCourierMainScreenRefreshStatusState.h"
#import "IZWaitingRequestsViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "IZGoogleMapsAPI.h"
#import "NSNotificationCenter+LocalNotifications.h"
#import "IZNewRequestsArrivedViewControllerDelegate.h"
#import "UIViewController+ChildViewController.h"
#import "IZMapViewDisplayManager.h"
#import "IZCourierMainScreenStrategy.h"
#import "IZCourierMainScreenVisibleStrategy.h"
#import "IZCourierMainScreenInvisibleStrategy.h"
#import "IZCourierMainScreenCollectRequestsStrategy.h"
#import "IZCourierMainScreenProcessingDeliveryStrategy.h"
#import "IZCourierMainScreenPickUpStrategy.h"
#import "IZCourierMainScreenDropOffStrategy.h"
#import "IZCourierMainScreenReturnStrategy.h"
#import "IZCourierMainScreenRefreshStatusStrategy.h"
#import "IZCourierMainScreenStrategyDelegate.h"
#import "IZNewRequestsArrivedViewController.h"
#import "IZDeliveryProcessViewController.h"
#import "IZPaymentConfirmationViewController.h"
#import "UINavigationItem+CustomButtons.h"
#import "IZDeliveryConfimationViewController.h"
#import "IZChangesInPackageViewController.h"
#import "IZReturnConfimationViewController.h"
#import "IZDrivingLicenseViewController.h"
#import "IZBadgeView.h"
#import "IZKeychain.h"
#import "NSString+Validation.h"

@protocol IZNewRequestArrivedDelegate;

static NSString *const IZBellIconName               = @"navigationBar_bell_icon";
static NSString *const IZSwitchIconName             = @"switchSelected";
static NSString *const IZSwitchIconUnselectName     = @"switchUnSelected";

typedef enum : NSUInteger {
    AfterLoginStepLicence,
    AfterLoginStepVehicle,
    AfterLoginStepPhone,
    AfterLoginStepBank,
    AfterLoginStepEnd,
} AfterLoginStep;

@interface IZCourierMainScreen () <IZWaitingRequestsViewControllerDelegate, IZLocationManagerObserver, IZNewRequestsArrivedViewControllerDelegate, IZCourierMainScreenStrategyDelegate, IZMapViewDisplayManagerDelegate, IZDeliveryProcessViewControllerDelegate, IZPaymentConfirmationViewControllerDelegate, IZDeliveryConfimationViewControllerDelegate, IZChangesInPackageViewControllerDelegate, IZReturnConfimationViewControllerDelegate, IZDrivingLicenseViewControllerDelegate>

@property (strong, nonatomic) id <IZCourierMainScreenState> currentState;
@property (weak, nonatomic) IZWaitingRequestsViewController *waitingRequestsViewController;
@property (weak, nonatomic) IZNewRequestsArrivedViewController *requestsArrivedViewController;
@property (weak, nonatomic) IZDeliveryProcessViewController *deliveryProcessViewController;
@property (weak, nonatomic) IBOutlet IZMapView *mapView;
@property (strong, nonatomic) IZMapViewDisplayManager *mapViewDisplayManager;
@property (strong, nonatomic) id <IZCourierMainScreenStrategy> currentStrategy;

@property (strong, nonatomic) IZUserService *userService;

@end

@implementation IZCourierMainScreen {
    id <IZCourierMainScreenState> _currentState;
}

@dynamic waitingRequestsViewController;
@dynamic requestsArrivedViewController;

#pragma mark - Action -

- (void)toggleInvisibleMode {
    [self.currentStrategy toggleVisibleMode];
}

- (void)showNotificationsScreen {
    [self.router showNotificationsScreenWithConfigurationBlock:nil];
}


#pragma mark - Lifecycle -

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [IZDataManager sharedManager].badgeManager.bellBadgeView = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self _setup];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([IZLocationManager sharedLocation].currentLocation) {
        [self.mapViewDisplayManager updateCourierMarkerPosition:[IZLocationManager sharedLocation].currentLocation direction:[IZLocationManager sharedLocation].direction];
        [self.mapViewDisplayManager firstUpdateCameraPositionWithLocation:[IZLocationManager sharedLocation].currentLocation];
    }
    [[IZDataManager sharedManager].badgeManager performUpdateUI];
}

#pragma mark - Getters -

- (id <IZCourierMainScreenState>)currentState {
    if (!_currentState) {
        _currentState = [[IZCourierMainScreenInvisibleState alloc] initWithClient:self];
    }
    return _currentState;
}

- (IZWaitingRequestsViewController *)waitingRequestsViewController {
    return (IZWaitingRequestsViewController *)[self childViewControllerWithClass:[IZWaitingRequestsViewController class]];
}

- (IZNewRequestsArrivedViewController *)requestsArrivedViewController {
    return (IZNewRequestsArrivedViewController *)[self childViewControllerWithClass:[IZNewRequestsArrivedViewController class]];
}

- (IZMapViewDisplayManager *)mapViewDisplayManager {
    if (!_mapViewDisplayManager) {
        _mapViewDisplayManager = [[IZMapViewDisplayManager alloc] initWithMapView:self.mapView];
        _mapViewDisplayManager.delegate = self;
    }
    return _mapViewDisplayManager;
}

- (IZDeliveryProcessViewController *)deliveryProcessViewController {
    return (IZDeliveryProcessViewController *)[self childViewControllerWithClass:[IZDeliveryProcessViewController class]];
}

- (IZUserService *)userService {
    if (!_userService) {
        if (![[IZKeychain authorizationToken] isEmpty]) {
            _userService = [[IZUserService alloc] initWithToken:[IZKeychain authorizationToken]];
        } else {
            _userService = [[IZUserService alloc] initWithToken:[IZDataManager currentUser].token];
        }
    }
    return _userService;
}

#pragma mark - <IZDrivingLicenseViewControllerDelegate> -

- (void)drivingLicenseViewControllerDidSave:(IZDrivingLicenseViewController *)viewController {
    NSLog(@"Error");
//    IZUser *user = [IZDataManager currentUser];
//    user.completeLicense = YES;
//    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)drivingLicenseViewControllerWantSkip:(IZDrivingLicenseViewController *)viewController {
    NSLog(@"Error");
//    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - <IZNewRequestsArrivedViewControllerDelegate> -

- (void)newRequestsArrivedViewController:(IZNewRequestsArrivedViewController *)viewController didAcceptRequest:(IZOrder *)request {
    [self.currentState setCollectRequestsState];
    request.status = IZOrderStatusWaitPickUp;
    [[IZDataManager sharedManager].myOrdersRepository addItem:request];
}

- (void)newRequestsArrivedViewController:(IZNewRequestsArrivedViewController *)viewController didRejectRequest:(IZOrder *)request {
    // do nothing
}

#pragma mark - <IZLocationManagerObserver> -

- (void)locationManager:(IZLocationManager *)locationManager didUpdateLocation:(CLLocation *)location {
    [self.mapViewDisplayManager updateCourierMarkerPosition:location direction:locationManager.direction];
    [self.mapViewDisplayManager firstUpdateCameraPositionWithLocation:location];
}

#pragma mark - <IZWaitingRequestsViewControllerDelegate> -

- (void)waitingRequestsViewControllerDidPressRequestsButton:(IZWaitingRequestsViewController *)viewController {
    __weak typeof(self) weakSelf = self;
    [self.router showNewRequestsArrivedScreenWithConfigurationBlock:^(IZControllerConfiguration *configuration) {
        __strong typeof(self) strongSelf = weakSelf;
        configuration.delegate = strongSelf;
        configuration.presenterViewController = strongSelf;
        configuration.presentationType = IZControllerPresentationTypeChildViewController;
    }];
}

- (void)waitingRequestsViewControllerDidPressStartDelivery:(IZWaitingRequestsViewController *)viewController {
    [self.currentStrategy toggleStartDelivery];
}

#pragma mark - <IZCourierMainScreenStrategyDelegate> -

- (void)courierMainScreenStrategyWantShowLoading:(id <IZCourierMainScreenStrategy>)strategy {
    [self showLoading];
}

- (void)courierMainScreenStrategyWantHideLoading:(id <IZCourierMainScreenStrategy>)strategy {
    [self hideLoading];
}

- (void)courierMainScreenStrategy:(id <IZCourierMainScreenStrategy>)strategy wantShowError:(NSError *)error {
    [self showAlertWithError:error buttonHandler:nil];
}

- (void)courierMainScreenStrategyDidSetVisibleMode:(id <IZCourierMainScreenStrategy>)strategy {
    [self.currentState setVisibleState];
}

- (void)courierMainScreenStrategyDidSetInvisibleMode:(id <IZCourierMainScreenStrategy>)strategy {
    [self.currentState setInvisibleState];
}

- (void)courierMainScreenStrategyDidStartDelivery:(id<IZCourierMainScreenStrategy>)strategy {
    [self.currentState setProcessingDeliveryState];
}
- (void)_reloadProfile{
    __weak typeof(self) weakSelf = self;
    if (![[IZKeychain authorizationToken] isEmpty]) {
        [self showLoading];
        
        [self.userService profileWithCompletion:^(id result, NSError *error) {
            [weakSelf hideLoading];
            if (!result || error) {
                return;
            }
            [IZDataManager setCurrentUser:result];
            IZUser *user = [IZDataManager currentUser];
            if ([user.status isEqualToString:@"review"]){
                [self showAlertWithMessage:LS(kBankDetailsSuccessMessageTextKey) title:LS(kBankDetailsSuccessTitleTextKey) OKButtonHandler:^(UIAlertAction *action) {
                }];
            }
            else{
                [weakSelf.currentStrategy toggleVisibleMode];
            }
        }];
    }
}

- (BOOL)courierMainScreenStrategyWillSetVisibleMode:(id <IZCourierMainScreenStrategy>)strategy {
    if ([self _verifyUserProfile]) {
        IZUser *user = [IZDataManager currentUser];
        if ([user.status isEqualToString:@"review"]){
            [self _reloadProfile];
            return NO;
        }
        return YES;
    }
#warning Please, check and fix the behavior on the registration
    __weak typeof(self) weakSelf = self;
    [self showAlertForSkipRegistrationWithYesButtonHandler:^(UIAlertAction *action) {
        [weakSelf _afterLoginSteps:AfterLoginStepLicence];
    } noButtonHandler:nil];

    return NO;
}

-(void)_afterLoginSteps:(NSInteger)step{
    __weak typeof(self) weakSelf = self;
    switch (step) {
        case AfterLoginStepLicence:{
            if (![weakSelf _showDrivingLicenseScreenIfNeeded]) {
                [self _afterLoginSteps:AfterLoginStepVehicle];
            }
        }
            break;
        case AfterLoginStepVehicle:{
            if (![weakSelf _showVehicleDetalLicenseScreenIfNeeded]){
                [weakSelf _afterLoginSteps:AfterLoginStepPhone];
            }
        }
            break;
        case AfterLoginStepPhone:{
            if (![weakSelf _showPhoneVerificationScreenIfNeeded]){
                [weakSelf _afterLoginSteps:AfterLoginStepBank];
            }
        }
            break;
        case AfterLoginStepBank:{
            if (![weakSelf _showBankDetailScreenIfNeeded]) {
                [weakSelf _afterLoginSteps:AfterLoginStepEnd];
            }
        }
            break;
        case AfterLoginStepEnd:{
            IZUser *user = [IZDataManager currentUser];
            bool isCompleet = YES;
            if (!user.completeLicense) {
                isCompleet = NO;
            }
            if (!user.completeVehicle) {
                isCompleet = NO;
            }
            if (!user.confirmPhone) {
                isCompleet = NO;
            }
            if (!user.completeBank) {
                isCompleet = NO;
            }
            if (!isCompleet){
                [self showAlertForSkipRegistrationWithYesButtonHandler:^(UIAlertAction *action) {
                    [weakSelf.router setupSideMenu];
                } noButtonHandler:^(UIAlertAction *action) {
                    [weakSelf _afterLoginSteps:AfterLoginStepLicence];
                }];
            }else{
                [weakSelf showAlertWithMessage:LS(kBankDetailsSuccessMessageTextKey) title:LS(kBankDetailsSuccessTitleTextKey) OKButtonHandler:^(UIAlertAction *action) {
                }];
            }
        }
            break;
        default:
            break;
    }
}

#warning delete?
- (void)showPostRegistrations:(NSUInteger)step{
    IZUser *user = [IZDataManager currentUser];
    if (!user.completeLicense) {
    }
}

- (void)courierMainScreenStrategy:(id <IZCourierMainScreenStrategy>)strategy didUpdateRequests:(NSArray <IZOrder *> *)requests {
    self.waitingRequestsViewController.requestCount = requests.count;
    if (self.requestsArrivedViewController) {
        if (requests.count) {
            [self.requestsArrivedViewController reload];
        } else {
            [self _dismissNewRequestArrivedViewController];
        }
    }
}

- (void)courierMainScreenStrategy:(id<IZCourierMainScreenStrategy>)strategy didUpdateCourierStatus:(IZCourierStatus *)status {
    [self _updateWaitingRequestsViewController];
    if (status.inProgress && status.acceptInProgress) {
        [self.currentState setProcessingDeliveryState];
    } else if (status.inProgress) {
        [self.currentState setStartDeliveryWarningState];
    } else if (status.orders.count) {
        [self.currentState setCollectRequestsState];
    } else if (!status.inProgress && !status.acceptInProgress) {
        [self.currentState setVisibleState];
    }
}

- (void)courierMainScreenStrategy:(id <IZCourierMainScreenStrategy>)strategy didUpdateDirection:(IZGMAPIDirectionResult *)direction {
    [self.mapViewDisplayManager updatePolylinesWithDirectionResult:direction];
}

- (void)courierMainScreenStrategy:(id <IZCourierMainScreenStrategy>)strategy wantUpdateMarkerWithDestinationPoints:(NSArray <IZDestinationPoint *> *)points {
    [self.mapViewDisplayManager updateRequestMarkers:points];
    [self _updateDeliveryPrecessViewController];
}

- (void)courierMainScreenStrategy:(id <IZCourierMainScreenStrategy>)strategy wantShowChangesInPackageScreenForRequest:(IZOrder *)request {
    __weak typeof(self) weakSelf = self;
    [self.router showChangesInPackageScreenWithRequestID:request.modelID configurationBlock:^(IZControllerConfiguration *configuration) {
        configuration.delegate = weakSelf;
//        configuration.setNavigationController = YES;
    }];
}

- (void)courierMainScreenStrategy:(id <IZCourierMainScreenStrategy>)strategy wantShowPaymentConfirmationForRequest:(IZOrder *)request {
    [self.navigationController popToRootViewControllerAnimated:YES];
    __weak typeof(self) weakSelf = self;
    [self.router showPaymentConfirmationWithOrder:request configurationBlock:^(IZControllerConfiguration *configuration) {
        configuration.presentationType = IZControllerPresentationTypePresentOnRoot;
        configuration.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        configuration.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        configuration.delegate = weakSelf;
    }];
}

- (void)courierMainScreenStrategyDidSuccessPickup:(id<IZCourierMainScreenStrategy>)strategy request:(IZOrder *)request {
    [self.router showAlertViewWithMessage:LS(kAlertsPickUpSuccessfullyKey) buttonTitle:LS(kAlertButtonContinue) buttonHandler:nil configurationBlock:^(IZControllerConfiguration *configuration) {
        configuration.presentationType = IZControllerPresentationTypePresentOnRoot;
        configuration.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        configuration.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    }];
    [self.currentState setProcessingDeliveryState];
}

- (void)courierMainScreenStrategy:(id<IZCourierMainScreenStrategy>)strategy wantShowDeliveryConfirmationForRequest:(IZOrder *)request {
    __weak typeof(self) weakSelf = self;
    [self.router showDeliveryConfirmationWithOrderID:request.modelID configurationBlock:^(IZControllerConfiguration *configuration) {
        configuration.presentationType = IZControllerPresentationTypePresentOnRoot;
        configuration.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        configuration.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        configuration.delegate = weakSelf;
    }];
}

- (void)courierMainScreenStrategyDidSuccessDropOff:(id<IZCourierMainScreenStrategy>)strategy request:(IZOrder *)request {
    __weak typeof(self) weakSelf = self;
    [self.router showAlertViewWithMessage:LS(kAlertsDropOffSuccessfullyKey) buttonTitle:LS(kAlertButtonContinue) buttonHandler:^{
        [weakSelf.router showRateAndReviewScreenWithRequestID:request.modelID hideBackButton:YES configurationBlock:nil];
    } configurationBlock:^(IZControllerConfiguration *configuration) {
        configuration.presentationType = IZControllerPresentationTypePresentOnRoot;
        configuration.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        configuration.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    }];
    if ([self _isCompleteDelivery]) {
        [self.currentState setInvisibleState];
    } else {
        [self.currentState setProcessingDeliveryState];
    }
}

- (void)courierMainScreenStrategyWantStartPickUp:(id<IZCourierMainScreenStrategy>)strategy {
    [self.currentState setPickUpState];
}

- (void)courierMainScreenStrategyWantStartDropOff:(id<IZCourierMainScreenStrategy>)strategy {
    [self.currentState setDropOffState];
}

- (void)courierMainScreenStrategyWantStartReturn:(id<IZCourierMainScreenStrategy>)strategy {
    [self.currentState setReturnState];
}

- (void)courierMainScreenStrategy:(id <IZCourierMainScreenStrategy>)strategy wantShowReturnConfirmationForRequest:(IZOrder *)request {
    __weak typeof(self) weakSelf = self;
    [self.router showReturnConfimationWithOrderID:request.modelID configurationBlock:^(IZControllerConfiguration *configuration) {
        configuration.presentationType = IZControllerPresentationTypePresentOnRoot;
        configuration.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        configuration.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        configuration.delegate = weakSelf;
    }];
}

- (void)courierMainScreenStrategyDidSuccessReturn:(id<IZCourierMainScreenStrategy>)strategy request:(IZOrder *)request {
    __weak typeof(self) weakSelf = self;
    [self.router showAlertViewWithMessage:LS(kAlertsReturnSuccessfullyKey) buttonTitle:LS(kAlertButtonContinue) buttonHandler:^{
        [weakSelf.router showRateAndReviewScreenWithRequestID:request.modelID hideBackButton:YES configurationBlock:nil];
    } configurationBlock:^(IZControllerConfiguration *configuration) {
        configuration.presentationType = IZControllerPresentationTypePresentOnRoot;
        configuration.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        configuration.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    }];
    if ([self _isCompleteDelivery]) {
        [self.currentState setInvisibleState];
    } else {
        [self.currentState setProcessingDeliveryState];
    }
}

#pragma mark - <IZCourierMainScreenStateClient> -

- (void)updateState:(id <IZCourierMainScreenState>)state {
    self.currentState = state;
    [self _updateUIByCurrentState];
    [self _updateStrategyByCurrentState];
    [self _updateMapViewPadding];
}

#pragma mark - <IZMapViewDisplayManagerDelegate> -

- (void)mapViewDisplayManager:(IZMapViewDisplayManager *)manager wantOpenRequestDetail:(IZOrder *)request {
    [self _showOrderDetailsScreenWithOrder:request];
}

#pragma mark - <IZDeliveryProcessViewControllerDelegate> -

- (void)deliveryProcessViewControllerWantOpenRequestDetail:(IZDeliveryProcessViewController *)viewController {
    IZDestinationPoint *point = [self.currentStrategy getNearestDestinationPoint];
    if (!point) {
        return;
    }
    IZOrder *order = [IZDataManager courierOrdersByModelID:point.orderID];
    if (!order) {
        return;
    }
    [self _showOrderDetailsScreenWithOrder:order];
}

- (void)deliveryProcessViewControllerDidPressDestination:(IZDeliveryProcessViewController *)viewController {
    [self.currentStrategy toggleAtDestinationButton];
}

- (void)deliveryProcessViewControllerWantCall:(IZDeliveryProcessViewController *)viewController {
    IZDestinationPoint *point = [self.currentStrategy getNearestDestinationPoint];
    if (!point) {
        return;
    }
    NSString *phoneNumber = [@"tel://" stringByAppendingString:point.contact];
    NSURL *url = [NSURL URLWithString:phoneNumber];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (void)deliveryProcessViewControllerDidTapHeader:(IZDeliveryProcessViewController *)viewController {
    [self.currentStrategy updateRoute];
}

- (void)deliveryProcessViewControllerDidPressCancel:(IZDeliveryProcessViewController *)viewController {
    [self.currentStrategy cancelReturn];
}

- (void)deliveryProcessViewControllerDidPressChat:(IZDeliveryProcessViewController *)viewController {
    IZDestinationPoint *destinationPoint = [self.currentStrategy getNearestDestinationPoint];
    if (!destinationPoint) {
        return;
    }
    IZOrder *order = [IZDataManager courierOrdersByModelID:destinationPoint.orderID];
    if (!order) {
        return;
    }
    [self.router showChatScreenWithOrder:order configurationBlock:nil];
}

#pragma mark - <IZPaymentConfirmationViewControllerDelegate> -

- (void)paymentConfirmationViewController:(IZPaymentConfirmationViewController *)viewController wantAcceptCode:(NSString *)code forRequestID:(NSString *)modelID {
    [self.navigationController popToRootViewControllerAnimated:YES];
    [self.currentStrategy acceptPickUpCode:code forReqeustID:modelID];
}

#pragma mark - <IZDeliveryConfimationViewControllerDelegate> -

- (void)deliveryConfimationViewController:(IZDeliveryConfimationViewController *)viewController didEnterConfirmation:(NSString *)code forRequstID:(NSString *)requestID {
    [self.currentStrategy acceptDeliveryCode:code forRequestID:requestID];
}

- (void)deliveryConfimationViewController:(IZDeliveryConfimationViewController *)viewController wantReturnRequstID:(NSString *)requestID {
    [self.currentStrategy returnRequestWithID:requestID];
}

#pragma mark - <IZChangesInPackageViewControllerDelegate> -

- (void)changesInPackageViewController:(IZChangesInPackageViewController *)viewController wantAcceptChangesWithConfigurator:(IZChangeOrderConfigurator *)configurator {
    [self.currentStrategy acceptChangesWithConfigurator:configurator];
}

#pragma mark - <IZReturnConfimationViewControllerDelegate> -

- (void)returnConfimationViewController:(IZReturnConfimationViewController *)viewController didEnterConfirmation:(NSString *)code forRequstID:(NSString *)requestID {
    [self.currentStrategy acceptReturnCode:code forRequestID:requestID];
}

#pragma mark - Private -

- (void)_setup {
    [self _initCurrentState];
    [self _setupLocationManager];
    [self _setupMapView];
}

- (void)_setupMapView {
    self.mapView.settings.myLocationButton = YES;
}

- (void)_initCurrentState {
    if ([IZDataManager currentUser].visible) {
        [self.currentState setRefreshStatusState];
    } else {
        [self.currentState setInvisibleState];
        [self updateState:self.currentState];
    }
}

- (void)_updateStrategyByCurrentState {
    if ([self.currentState isInvisibleState]) {
        [self _setInvisibleStrategy];
    } else if ([self.currentState isVisibleState]) {
        [self _setVisibleStrategy];
    } else if ([self.currentState isCollectRequestsState]) {
        [self _setCollectRequestsStrategy];
    } else if ([self.currentState isPickUpState]) {
        [self _setPickUpStrategy];
    } else if ([self.currentState isStartDeliveryWarningState]) {
        [self _setCollectRequestsStrategy];
    } else if ([self.currentState isProcessingDeliveryState]) {
        [self _setProcessingDeliveryStrategy];
    } else if ([self.currentState isDropOffState]) {
        [self _setDropOffStrategy];
    } else if ([self.currentState isReturnState]) {
        [self _setReturnStrategy];
    } else if ([self.currentState isRefreshStatusState]) {
        [self _setRefreshStatusStrategy];
    }
}

- (void)_setInvisibleStrategy {
    [self _setStrategyWithClass:[IZCourierMainScreenInvisibleStrategy class]];
}

- (void)_setVisibleStrategy {
    [self _setStrategyWithClass:[IZCourierMainScreenVisibleStrategy class]];
}

- (void)_setCollectRequestsStrategy {
    [self _setStrategyWithClass:[IZCourierMainScreenCollectRequestsStrategy class]];
}

- (void)_setPickUpStrategy {
    [self _setStrategyWithClass:[IZCourierMainScreenPickUpStrategy class]];
}

- (void)_setProcessingDeliveryStrategy {
    [self _setStrategyWithClass:[IZCourierMainScreenProcessingDeliveryStrategy class]];
}

- (void)_setDropOffStrategy {
    [self _setStrategyWithClass:[IZCourierMainScreenDropOffStrategy class]];
}

- (void)_setReturnStrategy {
    [self _setStrategyWithClass:[IZCourierMainScreenReturnStrategy class]];
}

- (void)_setRefreshStatusStrategy {
    [self _setStrategyWithClass:[IZCourierMainScreenRefreshStatusStrategy class]];
}

- (void)_setStrategyWithClass:(Class)class {
    id newStrategy = [[class alloc] initWithDelegate:self];
    self.currentStrategy = newStrategy;
}

- (void)_updateUIByCurrentState {
    [self _updateNavigationBarButtons];
    [self _dismissWaitinRequestsViewController];
    [self _dismissNewRequestArrivedViewController];
    [self _dismissDeliveryProcessViewController];
    if ([self.currentState isInvisibleState]) {
        [self _clearMapView];
    } else if ([self.currentState isVisibleState]) {
        [self _clearMapView];
        [self _presentWaitingRequestsViewController];
        [self _updateWaitingRequestsViewController];
    } else if ([self.currentState isCollectRequestsState]) {
        [self _presentWaitingRequestsViewController];
        [self _updateWaitingRequestsViewController];
    } else if ([self.currentState isPickUpState] || [self.currentState isDropOffState] || [self.currentState isReturnState]) {
        [self _presentDeliveryProcessViewController];
    } else if ([self.currentState isStartDeliveryWarningState]) {
        [self _presentStartDeliveryWarning];
    } else if ([self.currentState isReturnState]) {
        [self _clearMapView];
    }
    [self _updateDeliveryPrecessViewController];
    if ([self.currentState isStartDeliveryWarningState] ||
        [self.currentState isProcessingDeliveryState] ||
        [self.currentState isRefreshStatusState]) {
        return;
    }
    [self performNotificationCommandIfNeeded];
}

- (void)_updateMapViewPadding {
    self.mapView.padding = [self.currentStrategy mapViewPadding];
}

- (void)_clearMapView {
    [self.mapViewDisplayManager removeRoute];
    [self.mapViewDisplayManager removeActiveMarkers];
    [self.mapViewDisplayManager removeInactiveMarkers];
}

- (void)_updateNavigationBarButtons {
    self.navigationItem.rightBarButtonItem = nil;
    self.navigationItem.rightBarButtonItems = @[];
    [self _addNavigationBarButtonsIfNeeded];
}

- (void)_addNavigationBarButtonsIfNeeded {
    if (self.navigationItem.rightBarButtonItems.count) {
        return;
    }
    NSString *imageName = IZSwitchIconName;
    if ([self.currentState isInvisibleState]) {
        imageName = IZSwitchIconUnselectName;
    }
    if ([self.currentState isRefreshStatusState]) {
        // do nothing
    } else {
        [self.navigationItem addRightButtonWithImageName:imageName tintColor:nil target:self selector:@selector(toggleInvisibleMode)];
    }
    
    UIBarButtonItem *bellItem = [self.navigationItem addRightButtonWithImageName:IZBellIconName tintColor:nil target:self selector:@selector(showNotificationsScreen)];
    [IZDataManager sharedManager].badgeManager.bellBadgeView = [[IZBadgeView alloc] initWithBarButtonItem:bellItem];
}

- (BOOL)_verifyUserProfile {
    IZUser *user = [IZDataManager currentUser];
    if (!user.completeLicense) {
        return NO;
    }
    if (!user.completeVehicle) {
        return NO;
    }
    if (!user.confirmPhone) {
        return NO;
    }
    if (!user.completeBank) {
        return NO;
    }
    return YES;
}

- (BOOL)_showDrivingLicenseScreenIfNeeded {
    IZUser *user = [IZDataManager currentUser];
    if (!user.completeLicense) {
        __weak typeof(self) weakSelf = self;
        [self.router showDrivingLicenseScreenWithCompletion:^{
            [weakSelf.navigationController popToRootViewControllerAnimated:NO];
            [weakSelf _afterLoginSteps:AfterLoginStepVehicle];
        } configurationBlock:^(IZControllerConfiguration *configuration) {
            configuration.delegate = weakSelf;
        }];
        return YES;
    }
    return NO;
}

- (BOOL)_showVehicleDetalLicenseScreenIfNeeded {
    IZUser *user = [IZDataManager currentUser];
    if (!user.completeVehicle) {
        __weak typeof(self) weakSelf = self;
        [self.router showAddVehicleScreenWithCompletion:^{
            [weakSelf.navigationController popToRootViewControllerAnimated:NO];
            [weakSelf _afterLoginSteps:AfterLoginStepPhone];
        } configurationBlock:nil];
        return YES;
    }
    return NO;
}

- (BOOL)_showPhoneVerificationScreenIfNeeded {
    IZUser *user = [IZDataManager currentUser];
    if (!user.confirmPhone) {
        __weak typeof(self) weakSelf = self;
        [self.router showVerificationMobileNumberScreenWithCompletion:^{
            [weakSelf.navigationController popToRootViewControllerAnimated:YES];
            [weakSelf _afterLoginSteps:AfterLoginStepBank];
        } hideBackButton:NO configurationBlock:nil];
        return YES;
    }
    return NO;
}

- (BOOL)_showBankDetailScreenIfNeeded {
    IZUser *user = [IZDataManager currentUser];
    if (!user.completeBank) {
        __weak typeof(self) weakSelf = self;
        [self.router showBankDetailsScreenWithCompletion:^{
            [weakSelf.navigationController popToRootViewControllerAnimated:YES];
            [weakSelf _afterLoginSteps:AfterLoginStepEnd];
        } configurationBlock:nil];
        return YES;
    }
    return NO;
}

- (void)_presentWaitingRequestsViewController {
    if (self.waitingRequestsViewController) {
        return;
    }
    __weak typeof(self) weakSelf = self;
    [self.router showWaitingRequestScreenWithConfigurationBlock:^(IZControllerConfiguration *configuration) {
        __strong typeof(self) strongSelf = weakSelf;
        configuration.delegate = strongSelf;
        configuration.presenterViewController = strongSelf;
        configuration.presentationType = IZControllerPresentationTypeChildViewController;
    }];
}

- (void)_dismissWaitinRequestsViewController {
    [self.waitingRequestsViewController dismissChildViewController];
}

- (void)_dismissNewRequestArrivedViewController {
    [self.requestsArrivedViewController dismissChildViewController];
}

- (void)_dismissDeliveryProcessViewController {
    [self.deliveryProcessViewController dismissChildViewController];
}

- (void)_updateWaitingRequestsViewController {
    if (!self.waitingRequestsViewController) {
        return;
    }
    self.waitingRequestsViewController.deliveryTime = [IZDataManager courierStatus].deliveryTime;
    if ([self.currentState isVisibleState]) {
        self.waitingRequestsViewController.startTimer = NO;
        self.waitingRequestsViewController.deliveryTime = 0;
    } else if ([self.currentState isCollectRequestsState]) {
        self.waitingRequestsViewController.deliveryTime = [IZDataManager courierStatus].deliveryTime;
        self.waitingRequestsViewController.startTimer = YES;
    }
}

- (void)_updateDeliveryPrecessViewController {
    if (!self.deliveryProcessViewController) {
        return;
    }
    IZDestinationPoint *point = [self.currentStrategy getNearestDestinationPoint];
    if (!point) {
        return;
    }
    if ([self.currentState isPickUpState]) {
        self.deliveryProcessViewController.state = IZDeliveryProcessViewControllerStatePickUp;
    } else if ([self.currentState isDropOffState]) {
        self.deliveryProcessViewController.state = IZDeliveryProcessViewControllerStateDropOff;
    } else if ([self.currentState isReturnState]) {
        self.deliveryProcessViewController.state = IZDeliveryProcessViewControllerStateReturn;
    }
    self.deliveryProcessViewController.address = point.address;
}

- (void)_setupLocationManager {
//    [[IZLocationManager sharedLocation] requestLocation];
    [[IZLocationManager sharedLocation] addObserver:self];
}

- (void)_presentStartDeliveryWarning {
    __weak typeof(self) weakSelf = self;
    [self.router showWarningWithMessage:LS(kCourierShouldStartDeliveryWarningKey) buttonTitle:LS(kButtonRouteTitleKey) buttonHandler:^{
        [weakSelf.currentStrategy toggleStartDelivery];
    } configurationBlock:^(IZControllerConfiguration *configuration) {
        configuration.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        configuration.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        configuration.presentationType = IZControllerPresentationTypePresentOnRoot;
    }];
   
}

- (void)_presentDeliveryProcessViewController {
    __weak typeof(self) weakSelf = self;
    [self.router showDeliveryProcessScreenWithConfigurationBlock:^(IZControllerConfiguration *configuration) {
        __strong typeof(self) strongSelf = weakSelf;
        configuration.delegate = strongSelf;
        configuration.presenterViewController = strongSelf;
        configuration.presentationType = IZControllerPresentationTypeChildViewController;
    }];
}

- (BOOL)_isCompleteDelivery {
    if (![IZDataManager courierOrders].count) {
        [IZDataManager sharedManager].directionResult = nil;
        [IZDataManager sharedManager].destinationPoints = nil;
        return YES;
    }
    return NO;
}

- (void)_showOrderDetailsScreenWithOrder:(IZOrder *)order {
    IZRequestDetailsContentBuilder *builder = [[IZRequestDetailsContentBuilder alloc] initWithOrder:order];
    [builder addGallerySection];
    [builder addPickUpDropOffSection];
    [builder addPackageInfoSection];
    [builder addCustomerCommentSection];
    [builder addContactsSection];
    [self.router showOrderDetailsWithBuilder:builder title:LS(kRequestDetailsTitleKey) configurationBlock:nil];
}

- (void)_reloadProfileWithCompletion:(statusBlock)block {
    __weak typeof(self) weakSelf = self;
    if (![[IZKeychain authorizationToken] isEmpty]) {
        [self showLoading];

        [self.userService profileWithCompletion:^(id result, NSError *error) {
            [weakSelf hideLoading];
            if (!result || error) {
                block(NO,nil);
                return;
            }
            [IZDataManager setCurrentUser:result];
            block(YES,nil);
        }];
    }
}

@end
