//
//  IZChangesInPackageViewController.h
//  Jabrool
//
//  Created by Ilya Denisov on 1/25/17.
//  Copyright © 2017 Alex Linnik. All rights reserved.
//

#import "IZBaseViewController.h"

@class IZChangesInPackageViewController;

@protocol IZChangesInPackageViewControllerDelegate <NSObject>

- (void)changesInPackageViewController:(IZChangesInPackageViewController *)viewController wantAcceptChangesWithConfigurator:(IZChangeOrderConfigurator *)configurator;

@end

@interface IZChangesInPackageViewController : IZBaseViewController

@property (weak, nonatomic) id <IZChangesInPackageViewControllerDelegate> delegate;

- (instancetype)initWithRequestID:(NSString *)modelID;

@end
