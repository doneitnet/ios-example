//
//  IZDeliveryProcessViewController.h
//  Jabrool
//
//  Created by Ilya Denisov on 1/24/17.
//  Copyright © 2017 Alex Linnik. All rights reserved.
//

#import "IZBaseViewController.h"
#import "IZDestinationPoint.h"

typedef NS_ENUM(NSUInteger, IZDeliveryProcessViewControllerState) {
    IZDeliveryProcessViewControllerStatePickUp,
    IZDeliveryProcessViewControllerStateDropOff,
    IZDeliveryProcessViewControllerStateReturn
};

@class IZDeliveryProcessViewController;

@protocol IZDeliveryProcessViewControllerDelegate <NSObject>

- (void)deliveryProcessViewControllerWantOpenRequestDetail:(IZDeliveryProcessViewController *)viewController;
- (void)deliveryProcessViewControllerDidPressDestination:(IZDeliveryProcessViewController *)viewController;
- (void)deliveryProcessViewControllerWantCall:(IZDeliveryProcessViewController *)viewController;
- (void)deliveryProcessViewControllerDidTapHeader:(IZDeliveryProcessViewController *)viewController;
- (void)deliveryProcessViewControllerDidPressCancel:(IZDeliveryProcessViewController *)viewController;
- (void)deliveryProcessViewControllerDidPressChat:(IZDeliveryProcessViewController *)viewController;

@end

@interface IZDeliveryProcessViewController : IZBaseViewController

@property (weak, nonatomic) id <IZDeliveryProcessViewControllerDelegate> delegate;
@property (assign, nonatomic) IZDeliveryProcessViewControllerState state;
@property (strong, nonatomic) NSString *address;

@end
