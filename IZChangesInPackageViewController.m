//
//  IZChangesInPackageViewController.m
//  Jabrool
//
//  Created by Ilya Denisov on 1/25/17.
//  Copyright © 2017 Alex Linnik. All rights reserved.
//

#import "IZChangesInPackageViewController.h"
#import "IZChangesInPackageDataDisplayManager.h"
#import "IZBaseViewController+Protected.h"
#import "IZPickUpLocationViewController.h"
#import "IZGoogleMapsAPI.h"

@interface IZChangesInPackageViewController () <IZChangesInPackageDataDisplayManagerDataSource, IZChangesInPackageDataDisplayManagerDelegate, IZPickUpLocationViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IZChangesInPackageDataDisplayManager *dataDisplayManager;
@property (copy, nonatomic) NSString *requestID;
@property (strong, nonatomic) IZCourierService *courierService;
@property (strong, nonatomic) IZCourierOrderService *courierOrderService;
@property (strong, nonatomic) IZCatalogService *catalogService;
@property (strong, nonatomic) IZOrder *request;
@property (copy, nonatomic) void (^selectLocationCompletionBlock)(IZLocation *);
@property (strong, nonatomic) IZLocation *customLocation;
@property (assign, nonatomic) CGFloat customRoute;
@property (strong, nonatomic) IZGoogleMapsAPI *googleService;

@end

@implementation IZChangesInPackageViewController

#pragma mark - INITIALIZER -

- (instancetype)initWithRequestID:(NSString *)modelID {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _requestID = modelID;
    }
    return self;
}

#pragma mark - Lifecycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self _updateData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self _setupTitle];
}

#pragma mark - Getter -

- (IZChangesInPackageDataDisplayManager *)dataDisplayManager {
    if (!_dataDisplayManager) {
        _dataDisplayManager = [[IZChangesInPackageDataDisplayManager alloc] initWithTableView:self.tableView];
        _dataDisplayManager.dataDisplayManagerDataSource = self;
        _dataDisplayManager.delegate = self;
    }
    return _dataDisplayManager;
}

- (IZCourierService *)courierService {
    if (!_courierService) {
        _courierService = [[IZCourierService alloc] initWithToken:[IZDataManager currentUser].token];
    }
    return _courierService;
}

- (IZCourierOrderService *)courierOrderService {
    if (!_courierOrderService) {
        _courierOrderService = [[IZCourierOrderService alloc] initWithToken:[IZDataManager currentUser].token];
    }
    return _courierOrderService;
}

- (IZCatalogService *)catalogService {
    if (!_catalogService) {
        _catalogService = [[IZCatalogService alloc] initWithToken:[IZDataManager currentUser].token];
    }
    return _catalogService;
}

- (IZGoogleMapsAPI *)googleService {
    if (!_googleService) {
        _googleService = [IZGoogleMapsAPI new];
    }
    return _googleService;
}

#pragma mark - <IZPickUpLocationViewControllerDelegate> -

- (void)pickUpLocationViewController:(IZPickUpLocationViewController *)viewController didSelectDropOffLocation:(IZLocation *)location {
    CLLocation *recipientLocation = [location location];
    if (!recipientLocation) {
        return;
    }
    CLLocation *ownerLocation = [[CLLocation alloc] initWithLatitude:self.request.ownerLocation.latitude longitude:self.request.ownerLocation.longitude];
    [self showLoading];
    __weak typeof(self) weakSelf = self;
    [self.googleService distanceFromLocation:ownerLocation toLocation:recipientLocation completion:^(id result, NSError *error) {
        __strong typeof(self) strongSelf = weakSelf;
        [strongSelf hideLoading];
        if (!result || error) {
            [strongSelf showAlertWithError:error buttonHandler:nil];
            return;
        }
        IZGMAPIDistanceElement *distanceElement = result;
        strongSelf.customRoute = IZMetersToKilometers([distanceElement.distanceValue floatValue]);
        strongSelf.customLocation = location;
        if (strongSelf.selectLocationCompletionBlock) {
            strongSelf.selectLocationCompletionBlock(location);
        }
    }];
}

- (void)pickUpLocationViewController:(IZPickUpLocationViewController *)viewController didSelectPickUpLocation:(IZLocation *)location {
    
}

#pragma mark - <IZChangesInPackageDataDisplayManagerDataSource> -

- (NSInteger)changesInPackageDataDisplayManager:(IZChangesInPackageDataDisplayManager *)dataDisplayManager quantityForType:(IZPackageSizeType)sizeType {
    if (!self.request) {
        return 0;
    }
    return [self.request packageCountForPackageType:sizeType];
}

#pragma mark - <IZChangesInPackageDataDisplayManagerDelegate> -

- (void)changesInPackageDataDisplayManager:(IZChangesInPackageDataDisplayManager *)dataDisplayManager wantSelectLocationWithCompletion:(void(^)(IZLocation *))block {
    __weak typeof(self) weakSelf = self;
    [self.router showPickUpLocationScreenIsDropLocation:YES configurationBlock:^(IZControllerConfiguration *configuration) {
        configuration.setNavigationController = YES;
        configuration.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        configuration.presentationType = IZControllerPresentationTypePresentOnRoot;
        configuration.delegate = weakSelf;
    }];
    self.selectLocationCompletionBlock = block;
}

- (void)changesInPackageDataDisplayManagerDidAccept:(IZChangesInPackageDataDisplayManager *)dataDisplayManager {
    IZChangeOrderConfigurator *configurator = [IZChangeOrderConfigurator new];
    configurator.requestID = self.requestID;
    configurator.recipientLocation = self.customLocation ? self.customLocation.coordinate : self.request.recipientLocation;
    configurator.recipientAddress = self.customLocation ? self.customLocation.address : self.request.recipientAddress;
    configurator.route = self.customLocation ? self.customRoute : self.request.route;
    configurator.smallPackageCount = [self.dataDisplayManager getSmallPackageCount];
    configurator.mediumPackageCount = [self.dataDisplayManager getMediumPackageCount];
    configurator.largePackageCount = [self.dataDisplayManager getLargePackageCount];
    
    if ([self.delegate respondsToSelector:@selector(changesInPackageViewController:wantAcceptChangesWithConfigurator:)]) {
        [self.delegate changesInPackageViewController:self wantAcceptChangesWithConfigurator:configurator];
    }
}

#pragma mark - Private -

- (void)_setupTitle {
    self.title = LS(kChangesInPackageTitleKey);
}

- (void)_updateData {
    dispatch_group_t serviceGroup = dispatch_group_create();
    dispatch_queue_t queue = dispatch_queue_create("", DISPATCH_QUEUE_SERIAL);
    
    __block NSError *getPackagesError = nil;
    
    [self showLoading];
    dispatch_group_enter(serviceGroup);
    __weak typeof(self) weakSelf = self;
    dispatch_group_async(serviceGroup, queue, ^{
        [[IZDataManager sharedManager].packageTypesRepository getAllWithCompletionBlock:^(NSArray *result, NSError *error) {
            if (!result || error) {
                getPackagesError = error;
            }
            [weakSelf.dataDisplayManager updateTypes:result];
            dispatch_group_leave(serviceGroup);
        }];
    });
    
    __block NSError *getReqeustError = nil;
    dispatch_group_enter(serviceGroup);
    dispatch_group_async(serviceGroup, queue, ^{
        __strong typeof(self) strongSelf = weakSelf;
        [strongSelf.courierOrderService orderByModelID:strongSelf.requestID completion:^(id result, NSError *error) {
            if (!result || error) {
                getReqeustError = error;
            }
            strongSelf.request = result;
            dispatch_group_leave(serviceGroup);
        }];
    });
    
    dispatch_group_notify(serviceGroup, dispatch_get_main_queue(), ^{
        __strong typeof(self) strongSelf = weakSelf;
        [strongSelf hideLoading];
        if (getPackagesError || getReqeustError) {
            [strongSelf showAlertWithError:(getPackagesError ? getPackagesError : getReqeustError) buttonHandler:^(UIAlertAction *action) {
                [strongSelf.navigationController popViewControllerAnimated:YES];
            }];
            return;
        }
        [strongSelf.tableView reloadData];
    });
}

@end
